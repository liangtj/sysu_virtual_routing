import argparse
import datetime
import json

import select
import threading
import socket

import router
from util import log
from misc import *





test_ip = r'192.168.1.1'
test_ip = r'10.8.8.8'
test_ip = r'127.0.0.1'
test_ip = r''



def init(args):
    router.IP2Info[args.ip] = {'name': args.name, 'port': args.port, }


def main():
    TIME_Pattern = r'%Y%m%dT%H%M(%S)'

    SOURCE = 'config_test.json'
    TARGET = 'config_test___{}.json'.format(
        datetime.datetime.now().strftime(TIME_Pattern))

    parser = argparse.ArgumentParser()

    parser.add_argument('-i', '--input', type=argparse.FileType('r'))
    #                     default=SOURCE)
    # parser.add_argument('-o', '--output', type=argparse.FileType('a'),
    #                     default=TARGET)

    parser.add_argument('-a', '--ip', type=str, default="127.0.0.1")
    parser.add_argument('-p', '--port', type=int, default=2333)
    parser.add_argument('-n', '--name', type=str, default=AnonymousName)

    args = parser.parse_args()

    init(args)

    log(args)
    # log(router.IP2Info)

    if args.input:
        adjs = json.loads(args.input.read())
        for item in adjs:
            ip = item['ip']
            with socket.socket() as s:
                s.connect((ip, PORT))
                s.sendall(json.dumps({
                    'type': Packet.HELLO,
                    'data': 'Hello, world',}).encode())
                log('sent ...')
                recved_data = s.recv(1024)
            data = json.loads(recved_data)
            log('Received', data)
            try:
                ip = data['data'][0]
                socket.inet_aton(ip)
                args.ip = ip
                args.name = ip if args.name == AnonymousName else args.name
            except socket.error:
                log('error ip for local node:', ip)


    # r = router.Router(args.name)
    r = router.Router(router.Name2IP['A'])
    r.run()


if __name__ == '__main__':
    main()
