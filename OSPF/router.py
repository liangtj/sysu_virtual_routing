import json
import collections
import math

import socket
import asyncore
import asynchat

import networkx as nx
import matplotlib.pyplot as plt

from util import log, Timer, T
from misc import *


IP_Default = r'127.0.0.1'

INTERVAL_CHECK_CHANGE = 3  # s
INTERVAL_ADJs_BROADCAST = 1  # s


# class Config(dict):

IP2Info = {
    '192.168.199.101': {'name': 'Sky',
                        'PORT': 2333, },
    '192.168.221.128': {'name': 'VM-binly',
                        'PORT': 2333, },

    '192.168.1.1': {'name': 'A',
                    'PORT': 2333, },
    '192.168.1.2': {'name': 'B',
                    'PORT': 2333, },
    '192.168.1.3': {'name': 'C',
                    'PORT': 2333, },
    '192.168.1.4': {'name': 'D',
                    'PORT': 2333, },
    '192.168.1.5': {'name': 'E',
                    'PORT': 2333, },
}
Name2IP = {}
for ip, info in IP2Info.items():
    name = info.get('name', AnonymousName)
    Name2IP[name] = ip


# class Client(asyncore.dispatcher):
#     '''Client'''


class Router(asyncore.dispatcher):
    '''Router'''

    def __init__(self, name, port=PORT, router=None):
        asyncore.dispatcher.__init__(self)
        self.create_socket()  # default TCP, whatever
        self.set_reuse_addr()
        self.bind(('', port))
        self.listen(5)  # originally 1

        self.name = name
        self.address = None

        self.router = router
        self.topo = Topo()
        self.routing_table = {}
        self.topo_changed = False
        self.routing_table_changed = False

        self.timers = {}
        self.connections = {}

        self.buffer = ''

    def run(self):
        self.timers['hello'] = Timer(
            INTERVAL_ADJs_BROADCAST, self.say_hello, repeat=True)
        self.timers['check'] = Timer(
            INTERVAL_CHECK_CHANGE, self.gen, args=(Topo(adjacents_1),), repeat=True)

        for t in self.timers.values():
            t.start()

        asyncore.loop()

    # def writable(self):

    def handle_close(self):
        self.close()
        # for conn in list(self.connections.values()):
        log('{} down'.format(self.name, ))

    def handle_accepted(self, sock, addr):
        print('Incoming connection from {}'.format(repr(addr)))
        EchoHandler(sock, data=addr)

    def route(self, destination):
        dest = destination
        if dest in self.routing_table:
            return self.routing_table[dest]

        return IP_Default

    def update(self, destination, new_next):
        self.routing_table[destination] = new_next

    def reset(self):
        self.routing_table = {}

    def gen(self, topo):
        def prev_of(x): return list(prev[x].keys())[0]

        start = Name2IP.get(self.name, self.name)
        if start == AnonymousName:
            log('*')
            return
        prev = dijksta(topo.adjs, start)
        for n in prev.keys():
            hop = n
            while prev_of(hop) != start:
                hop = prev_of(hop)
            # self.update(RouterIP[n]['IP'], RouterIP[hop]['IP'])
            self.update(n, hop)

        log(self.routing_table)

    def relatives(self):
        return self.routing_table.keys()

    def say_hello(self):
        for ip in self.relatives():
            packet = {'adjs': adjacents_1, }
            # log('-', end='')
            # self.push(packet, (self.route(ip), PORT))


class EchoHandler(asyncore.dispatcher_with_send):

    def __init__(self, sock=None, map=None, data=None):
        asyncore.dispatcher_with_send.__init__(self, sock=sock, map=map)
        self.data = data

    def handle_read(self):
        recved_data = self.recv(8192)
        if not recved_data:
            return
        # FIXME: if no `.decode()`, error
        text = json.loads(recved_data.decode())
        if text['type'] == Packet.HELLO:
            self.send(json.dumps({
                'type': Packet.HELLO_RES,
                'data': self.data,
            }).encode())
        else:
            self.send(recved_data)


class Topo(object):
    '''Topology'''

    def __init__(self, graph={}):
        self.adjs = graph

    def load_from_file_as_edges(self, filename):
        try:
            with open(filename) as f:
                config = json.loads(f)
                self.adjs = config.get('adjs', None)

                # TODO alias nodes' names
                # names = config.get('names', {})

                # NOTE: adapt to be Exception ?
                if not validate(self.adjs):
                    raise 'invalid graph: {}'.format(self.adjs)
        except IOError as err:
            log(err)

    def add(self, *edges):
        '''edge := (nodeA, nodeB, cost)'''

        d = collections.defaultdict(dict)
        for e in edges:
            node0, node1, cost = e
            d[node0][node1] = cost
            d[node1][node0] = cost
        self.adjs.update(dict(d))

    def down(self, *edges):
        '''edge := (nodeA, nodeB, ...)'''

        for e in edges:
            node0, node1, *_ = e
            if node0 not in self.adjs:
                raise 'remove a nonexistent node({}) from topo({})'.format(
                    node0, self.adjs)
            del self.adjs[node0][node1]
            del self.adjs[node1][node0]

    def draw(self):
        G = nx.Graph()

        edges = self.adjs
        for node in edges:
            for other in edges[node]:
                G.add_edge(node, other, weight=edges[node][other])

        pos = nx.spring_layout(G)
        # pos = nx.get_node_attributes(G, 'pos')  # this seems require position value for all node
        nx.draw(G, pos)

        nx.draw_networkx_nodes(G, pos, node_size=700)

        nx.draw_networkx_edges(G, pos)

        nx.draw_networkx_labels(G, pos, font_size=20, font_family='sans-serif')

        labels = nx.get_edge_attributes(G, 'weight')
        nx.draw_networkx_edge_labels(G, pos, edge_labels=labels)

        plt.axis('off')
        plt.show()


def validate(graph, correct=False):
    if correct:
        log(' ... TODO ... ')

    for node0 in graph:
        adjs = graph[node0]
        for node1 in adjs:
            cost = graph[node0][node1]
            if not graph[node1] or graph[node1][node0] != cost:
                return False
    return True


def dijksta(edges, start_node):

    if not validate(edges):
        raise 'invalid graph: {}'.format(edges)

    visiteds = set()
    unvisiteds = set(edges.keys())
    prev = {}

    distances = {}
    for node in edges:
        distances[node] = math.inf

    adjs = edges[start_node]
    for node in adjs:
        distances[node] = adjs[node]
        prev[node] = {start_node: adjs[node]}

    distances[start_node] = 0
    visiteds.add(start_node)

    # for n in unvisiteds:
    # FIXME: prevent loop when not strong-connect ...
    while len(unvisiteds) > 0:
        current_node = min(unvisiteds, key=lambda x: distances[x])

        for current_adj in edges[current_node]:
            new_dist = distances[current_node] + \
                edges[current_node][current_adj]
            if new_dist < distances[current_adj]:
                distances[current_adj] = new_dist
                prev[current_adj] = {current_node: new_dist}

        visiteds.add(current_node)
        unvisiteds.discard(current_node)

    # NOTE: need assert `len(prev)` ?
    # FIXME: the along-cost would be showed incorrectly
    # Topo(prev).draw()

    return prev


adjacents_0 = {
    'A': {'B': 3, 'C': 5, 'D': 6, 'E': 7},
    'B': {'A': 3, 'D': 13, 'C': 1, },
    'C': {'A': 5, 'B': 1, },
    'D': {'A': 6, 'B': 13, },
    'E': {'A': 7, },
}
adjacents_1 = {
    '192.168.1.1': {'192.168.1.2': 3, '192.168.1.3': 5, '192.168.1.4': 6, '192.168.1.5': 7},
    '192.168.1.2': {'192.168.1.1': 3, '192.168.1.4': 13, '192.168.1.3': 1, },
    '192.168.1.3': {'192.168.1.1': 5, '192.168.1.2': 1, },
    '192.168.1.4': {'192.168.1.1': 6, '192.168.1.2': 13, },
    '192.168.1.5': {'192.168.1.1': 7, },
}


def main():
    # # ...
    # Topo(dijksta(adjacents_1, '192.168.1.1')).draw()
    # Topo(dijksta(adjacents_1, Name2IP['A'])).draw()

    # t = Topo()
    # t.add(('A', 'B', 1), ('B', 'C', 3))
    # t.draw()
    # t.add(('A', 'E', 1), ('A', 'C', 3))
    # t.draw()
    # t.down(('A', 'C', 100))
    # t.draw()

    # r.run()
    # asyncore.loop()
    pass


if __name__ == '__main__':
    main()
