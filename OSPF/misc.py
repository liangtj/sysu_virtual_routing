import json
import collections
import math


from util import log

BROADCAST_ip = r'255.255.255.255'

AnonymousName = '**'

PORT = 2333


class Message():
    '''Message'''

    def __init__(self, header, body):
        self.header = header
        self.body = body


class Packet():

    HELLO = 'HELLO'
    HELLO_RES = 'HELLO_RES'
    LS = 'LS'


class HelloPacket(Packet):
    pass
