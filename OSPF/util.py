import time
import sched
import threading


log = print

# t = sched.scheduler(time.time, time.sleep)
# def timer(interval, action, args=(), repeat=False):
#     t = sched.scheduler(time.time, time.sleep)
#     if not repeat:
#         t.enter(interval, 1, action, args)
#     else:
#         def repeat_wrap(args):
#             action(*args)
#             t.enter(interval, 1, repeat_wrap, (args,))
#         t.enter(interval, 1, repeat_wrap, (args,))
#     return t


class Timer(threading.Timer):

    def __init__(self, interval, action, args=None, kwargs=None, repeat=False):
        threading.Timer.__init__(self, interval, action, args=args, kwargs=kwargs)
        self.repeat = repeat

    def run(self):
        while not self.finished.is_set():
            self.finished.wait(self.interval)
            self.function(*self.args, **self.kwargs)
            if not self.repeat:
                self.finished.set()


class T():

    def __init__(self, interval, action, args=None, kwargs=None, repeat=False):
        if not repeat:
            self.timer = threading.Timer(interval, action, args=args, kwargs=kwargs)
        else:
            def repeat_wrap(*args, **kwargs):
                action(*args, **kwargs)
                self.timer = threading.Timer(interval, repeat_wrap, args=args, kwargs=kwargs)
                self.timer.start()

            self.timer = threading.Timer(interval, repeat_wrap, args=args, kwargs=kwargs)

    def start(self):
        self.timer.start()

    def cancel(self):
        self.timer.cancel()

